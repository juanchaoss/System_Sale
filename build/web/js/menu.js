
$(document).ready(function () {


    function ajaxLoad(url, data, success)
    {
        $.ajax({
            url: url,
            type: 'POST',
            data: {opt: data},
            success: function (data, textStatus, jqXHR)
            {
                $("#contenedor").empty();
                $("#contenedor").append(data);
            }
        });

    }
    $(document).on("click", "#Kino", function () {
        ajaxLoad("./IndexServlet", "Kino", "");
    });

    $(document).on("click", "#Reportes", function () {
        ajaxLoad("./IndexServlet", "Reportes", "");
    });
    
      $(document).on("click", "#Lottoleon", function () {
        ajaxLoad("./IndexServlet", "Lottoleon", "");
    });
      $(document).on("click", "#ReportesLotto", function () {
        ajaxLoad("./IndexServlet", "ReportesLotto", "");
    });

  
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var datetime = null,
            date = null;

    var update = function () {
        date = moment(new Date());
        datetime.html(date.format('h:mm A'));
    };
//Current Date
    var dt = new Date(),
            year = dt.getFullYear(),
            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            month = dt.getMonth(),
            days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            day = dt.getDay(),
            date = dt.getDate();

    $('.year').append(year);
    $('.month').append(months[month]);
    $('.weekday').append(days[day]);
    $('.date').append(date);


//Current Time
    datetime = $('.current-time');
    update();
    setInterval(update, 60000);

});

