/* global printM */

$(document).ready(function() 
{
    function confirmSwal(text, type, yes, no, id, bool,cancel)
    {     
        if(cancel==='')
        {
            cancel=true;
        }
        swal({
            title: "Transacci\u00f3n en proceso",  
            text: text,   
            type: type,   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true,
            closeOnCancel: cancel
        }, 
        function(isConfirm)
        {   
            if(isConfirm)
            { 
                console.log(yes);
                if(id)
                {
                    console.log(yes);
                    yes(id, bool);
                }
                else
                {
                    yes;
                }
            }
            else
            {
                console.log(no);
                if(id && bool===true)
                {
                    no(
                        'Desea que se envíe por correo su comprobante?',
                        'info',
                        createModalInput,
                        createModalPrint, 
                        'n'+id, 
                        false,
                        true
                    );
                }
                else if(id && bool===false)
                {
                    no('pModal');
                }
                else
                {    
                    no;
                }
            }
        });
    }
    function beforePrint(id)
    {
        confirmSwal
        (
            'Desea sumunistar sus datos?',
            'info'
            ,createModalInput
            ,confirmSwal
            ,id
            ,true
            ,false
        );
    }
    function createModalInput(id, insert)
    {
        console.log(id);
        var html='';
        html+='<div class="modal fade bd-example-modal-lg"  id="'+id+'" tabindex="-1" role="dialog" style="font-size: 9px;">';
        html+='<div class="modal-dialog modal-lg" role="document">';
            html+='<div class="modal-content">';
                html+='<div class="modal-header">';
                    html+='<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    html+='<h4 class="modal-title hidden-print">Registro de cliente</h4>';
                html+='</div>';
                html+='<div class="modal-body">';
                   html+='<div class="panel panel-default ">';
                       html+='<div class="panel-body">';
                           html+='<div class="form-group hidden-print">';
                           if(insert===true)
                           {
                               html+='<input type="text" class="form-control" id="firstName" name="firstName">';
                               html+='<input type="text" class="form-control" id="lastName" name="lastName">';
                               html+='<input type="text" class="form-control" id="dni" name="dni">';
                           }
                           else
                           {
                                html+='<input type="email" class="form-control" id="email" name="email">';
                           }
                           html+='</div>';
                       html+='</div>';
                   html+='</div>  ';
               html+='</div>';
               html+='<div class="modal-footer">';
                   html+='<button type="button" class="btn btn-default hidden-print" data-dismiss="modal">Cerrar</button>';
                    html+='<button type="button" id="next" class="btn btn-success hidden-print">Siguiente</button>';
               html+='</div>';
           html+='</div><!-- /.modal-content -->';
       html+='</div><!-- /.modal-dialog -->';
   html+='</div><!-- /.modal -->';
   if(!$('#'+id).length)
   {
    $('.panel').append(html);   
   } 
   return $('#'+id).modal('show');
    }
    function createModalPrint(id)
    {
        var html='';
        html+='<div class="modal fade bd-example-modal-lg"  id="'+id+'" tabindex="-1" role="dialog" style="font-size: 9px;">';
        html+='<div class="modal-dialog modal-lg" role="document">';
            html+='<div class="modal-content">';
                html+='<div class="modal-header">';
                    html+='<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    html+='<h4 class="modal-title">Detalle de venta</h4>';
                html+='</div>';
                html+='<div class="modal-body">';
                   html+='<div class="panel panel-default">';
                       html+='<div class="panel-body">';
                           html+='<div class="form-group';
                               html+='<div class="col-lg-3">';
                                   html+='<span class="" id="nFact" name="nFact"><b>Nº de Factura:</b> 0000001</span>';
                               html+='</div>';
                               html+='<div class="col-lg-3">';
                                   html+='<span class="" id="nFact" name="nFact"><b>Fecha:</b> 18/04/2016</span>';
                               html+='</div>';
                               html+='<div class="col-lg-3">';
                                     html+='<span class="" id="nFact" name="nFact"><b>Agencia:</b> La bendicion de dios,c.a</span>';
                               html+='</div>';
                               html+='<div class="col-lg-3">';
                                     html+='<span class="" id="nFact" name="nFact"><b>Direccion:</b>4to de mayo,lallllala</span>';
                               html+='</div>';
                               html+='<br><br>';
                               html+='<div class="col-lg-12">';
                                   html+='<div class="table-responsive">';
                                   html+='<table class="table table-condensed table-bordered">';
                                       html+='<thead>';
                                   html+='<tr>';
                                       html+='<th>Nº Comprobante</th>';
                                       html+='<th>Sorteo</th>';
                                       html+='<th>Nº de tickets</th>';
                                       html+='<th>Miniserial</th>';
                                       html+='<th>Numeros</th>';
                                       html+='<th>Precio</th>';
                                   html+='</tr>';
                               html+='</thead>';
                               html+='<tbody>';
                                   html+='<tr>';
                                       html+='<td>000000008</td>';
                                       html+='<td>1251-11/07/2016</td>';
                                       html+='<td>1</td>';
                                       html+='<td>559</td>';
                                       html+='<td>010203040506070809101112131415</td>';
                                       html+='<td>400</td>';
                                   html+='</tr>';
                               html+='</tbody>';
                                   html+='</table>';
                                   html+='</div>';
                               html+='</div>';
                               html+='<div class="col-lg-12">';
                                     html+='<span class="form-control" id="nFact" name="nFact"><b>TOTAL: </b>BS.400</spa>';
                               html+='</div>';
                               html+='<br><br>';
                           html+='</div>';
                       html+='</div>';
                   html+='</div>  ';
               html+='</div>';
               html+='<div class="modal-footer hidden-print">';
                   html+='<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>';
                    html+='<button type="button" id="print" onclick="print();" class="btn btn-success"><i class="fafa-print"></i>&nbsp;Imprimir</button>';
               html+='</div>';
           html+='</div><!-- /.modal-content -->';
       html+='</div><!-- /.modal-dialog -->';
   html+='</div><!-- /.modal -->';
   if(!$('#'.id).length)
   {
    $('.panel').append(html);
   }
   return $('#'+id).modal('show');
    }
    function alertSwal(title, text, type)
    {
        swal(title, text, type);
    }
    function sendAjax(url, type, data, done, bSend, fail)
    {
        $.ajax({
                    url: url,
                    type: type,
                    data: data,
                    async:false,
                    cache:false,
                    success: function(output) 
                    {
                        if(output.ok!==undefined)
                        {
                            alertSwal('Exitoso',output.ok,'success');
                            removeAll();
                            $('#tKcomp').children('tbody').remove();
                        }
                        else if(output.error!==undefined)
                        {
                            alertSwal('Error',output.error,'error');   
                            removeAll();
                            $('#tKcomp').children('tbody').remove();
                        }
                        else
                        {
                            if(done==='comp')
                            {
                                $('#tKcomp tr').last().children('td:eq(2)').html(output);
                            }
                        }
                    },
                    fail:function() 
                    {
                        console.log("error");
                        fail;
                    },
                    beforeSend:function() 
                    {
                        console.log("complete");
                        bSend;
                    }
                });
    }
    function assignVal(rnd)
    {
        //Genera la asignación de valores del miniserial.
        if(rnd===true)
        {
            $('#focusedInput').val(Math.floor((Math.random() * 1000)));
        }
        var play=$('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').html();
        var playArr=play.split(',');
        $('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').html(playArr.sort().toString());
        $('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(1)').html($('#focusedInput').val());                          
        $('#tKcomp').append('<tr id="tK'+($('#tKcomp tbody tr').length)+'"><td>'+($('#tKcomp tbody tr').length+1)+'</td><td></td><td></td><td><i class="fa fa-times" aria-print="true"></i></td></tr>');
        removeAll();
        alertSwal('Transacci\u00f3n exitosa', '', 'success');

    }
    function removeAll()
    {
        //Elimina la clase que define que una esfera está seleccionada.
        $('div[class="btn round-green"]').removeClass('round-green').addClass('round');
        $('#focusedInput').val('');
    }
    function removePlay(res)
    {
        //Retorna el número que se quitó de la selección en la jugada.
        var str=$.trim($('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').html());
        var arr=str.split(',');
        var resp=$.grep(arr, function(invarr)
        {
            return invarr===res;
        }, true);
        if(resp[13]!=='' && resp[13]!== undefined)
        {
            $('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').html(resp.toString()+',');
        }
        else if(resp[12]!=='' && resp[12]!== undefined)
        {
            $('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').html(resp.toString());
        }
        else
        {
            $('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').html(resp.toString());
        } 
        //-----------------------------------------------------------
    }
    function verTr()
    {
        //Verifica si existe un tr en la tabla de las jugadas
        tr=$('#tKcomp').children('tbody tr').length;
        if($('#tK'+tr).length===0)
        {
            $('#tKcomp').append('<tr id="tK'+$('#tKcomp').children('tbody tr').length+'"><td>'+($('#tKcomp').children('tbody tr').length+1)+'</td><td></td><td></td><td><i class="fa fa-times" aria-hidden="true"></i></td></tr>');
        }
    }
    function mapTable()
    {
        // Recorre la tabla creando un arreglo de objetos por cada tr.
        var tbl = $('table#tKcomp tr:has(td)').map(function(i, v) 
        {
            var td =  $('td', this);
            if($(td).eq(2).html()!=='')
            {
            var playStr=td.eq(2).text().split(',').join('');
                return{
                         id: td.eq(0).text(),
                         miniSerial: td.eq(1).text(),
                         play: playStr               
                };
            }
        }).get();
        return tbl;
    }
    $(document).on('click','#next',function()
    {
        createModalPrint('printModal'); 
    });
    $("#dni").keydown(function (e)
    {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
        {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) 
        {
            e.preventDefault();
        }
    });
  
    $(document).on('click', 'div[class="btn round"],[class="btn round-green"]', function()
    {
        var res=$(this).html();            
        if($('div[class="btn round-green"]').length<15)
        {
            verTr();
            if($(this).attr('class')==='btn round')
            {
                $(this).removeClass('round');
                $(this).addClass('round-green');          
                if($('div[class="btn round-green"]').length===15)
                {
                    $('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').append($(this).html());                
                }
                else
                {
                    $('tr[id="tK'+($('#tKcomp tbody tr').length-1)+'"] td:eq(2)').append($(this).html()+',');                                    
                }
            }
            else
            {
                $(this).removeClass('btn round-green').addClass('btn round');;
                removePlay(res);
            }
        }
        else if($(this).attr('class')!=='btn round')
        {
            $(this).removeClass('round-green').addClass('round');
            removePlay(res);
        }
    });
    $(document).on('click','#btnProc', function()
    {
        cont=$('#tKcomp tr').last().children('td:eq(2)').html();
        op=$('div[class="btn round-green"]');
        if(op[14]==='')
        {
            op=op.splice(0,14);
        }
        if($('#focusedInput').val()==='' && op.length===15)
        {
            confirmSwal('Si deja el miniserial vac\u00edo se genera autom\u00e1ticamente', 'warning', assignVal(true),alertSwal('Transacci\u00f3n rechazada', 'No se ha realizado ninguna operación.', 'success'));
            op='';
        }
        else if($('#focusedInput').val()==='' && op.length===0)
        {
            sendAjax('./ClientSaleServlet', 'POST', {action:'getRandom'},'comp');
        }
        else if($('#focusedInput').val()!=='' && op.length===0)
        {
            sendAjax('./ClientSaleServlet', 'POST', {action:'getRandom'},'comp');
        }
       else if($('#focusedInput').val()!=='' && op.length===15)
       {
            verTr();
            assignVal(false);
            alertSwal("Procesado!", "Se ha registrado la jugada", "success");
            op='';
        }
        else
        {
            alertSwal('Error','Debe elegir todos los n\u00fameros de la jugada','warning');
        }
    });
    $(document).on('click', 'i',function()
    {
        tr=$('#tKcomp tbody tr').length;
        if(tr>2)
        {
            if($(this).parents('tr').children('td:eq(1)').html()!=='' && $(this).parents('tr').children('td:eq(2)').html()!=='')
            {
                if($(this).parents('tr').next('tr').length)
                {
                    /*
                    console.log($(this).parents('tr').nextAll('tr').attr('id'));
                    $(this).parents('tr').nextAll('tr').attr('id','tk'+($(this).parents('tr').nextAll('tr').index()-1));
                    console.log($(this).parents('tr').nextAll('tr').attr('id'));
                    $(this).parents('tr').nextAll('tr').eq(0).html(($(this).parents('tr').nextAll('tr').children('td').eq(0)-1));
                    $(this).parents('tr').remove();
                    */
                    $(this).parents('tr').nextAll().each(function()
                    {
                        $(this).attr('id','tK'+($(this).index()-1));
                        $(this).children('td').eq(0).html($(this).index());
                    });
                    $(this).parents('tr').remove();
                    
                }
                else
                {
                    $(this).parents('tr').remove();
                }
                removeAll();
            }
        }
        else
        {
            $(this).parents('tbody').remove();
            $('#tKcomp').append('<tbody><tr id="tK'+$('#tKcomp').children('tbody tr').length+'"><td>'+($('#tKcomp').children('tbody tr').length+1)+'</td><td></td><td></td><td><i class="fa fa-times" aria-hidden="true"></i></td></tr></tbody>');
            removeAll();
        }
    });
    $(document).on('click','#cancel',function()
    {
        removeAll();
        $('#tKcomp').children('tbody').remove();
    });
    $(document).on('click', '#sell', function()
    {
        if($('#tKcomp tr').length>1 && $('#tKcomp tr').last().children('td:eq(1)').html!=='')
        {
            beforePrint('insertClient');
            //sendAjax('./ClientSaleServlet', 'POST', {jsonString: JSON.stringify(mapTable()),action:'insert'});
        }
        else if($('#tKcomp tr').last().prev().children('td:eq(1)').html!=='' && $('#tKcomp tr').length>1)
        {
            alertSwal('Error', 'Su jugada aún no tiene una serie completa', 'warning');
        }
        else
        {
            alertSwal('Error', 'Debe generar alguna jugada antes de poder vender', 'warning');
        }
    });
});