<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
     <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.png">

   
    <title>Mycrocom - Sistema de ventas</title>
    <!-- Google-Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
      <link href="./css/bootstrap.css" rel="stylesheet">
 
    <!--Animation css-->
    <link href="./css/animate.css" rel="stylesheet">
    <!--Icon-fonts css-->
    <link href="./css/font-awesome.css" rel="stylesheet" />
    <link href="./css/ionicons.min.css" rel="stylesheet" />
    <!-- sweet alerts -->
    <link href="./css/sweet-alert.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/hover.css" rel="stylesheet">
     <link href="./css/VentaKino.css" rel="stylesheet">
<!--      <link href="./css/bootstrap-reset.css" rel="stylesheet">-->
 
    
    <link href="./css/helper.css" rel="stylesheet">
    <link href="./css/style-responsive.css" rel="stylesheet" />
    <link href="./css/dataTables.bootstrap.min.css" rel="stylesheet">
   
    

</head>
    <body>
     <jsp:include page="/WEB-INF/dashboard/Menu.jsp" />

            <!--Main Content Start -->
            <section class="content">

                <!-- Header -->
                <header class="top-head container-fluid  hidden-print">
                    <button type="button" class="navbar-toggle pull-left">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Right navbar -->
                    <ul class="list-inline navbar-right top-menu top-right-menu">  
                        <!-- mesages -->  
                        <!-- user login dropdown start-->
                     
                        <li class="dropdown text-center">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <img alt="" src="img/user-image.png" class="img-circle profile-img thumb-sm">
                                <span class="username">Agencia</span> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">

                                <li><a href="#"><i class="fa fa-sign-out"></i> Log Out</a></li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->       
                    </ul>
                    <!-- End right navbar -->

                </header>
                <!-- Header Ends -->

                <!-- Page Content Start -->
                <!-- ================== -->

                <div id="contenedor" name="contenedor" class="wraper container-fluid">
                    <img class="center-block" src="img/mycrocomsombra.png">

                </div>
                <!-- Page Content Ends -->
                <!-- ================== -->

                <!-- Footer Start -->
                <footer class="footer hidden-print">
                    2016 � Mycrocom Sistemas, c.a.
                </footer>
              

                <!-- Footer Ends -->
            </section>



      
    <!-- section javascript -->
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/modernizr.min.js"></script>
    <script src="./js/pace.min.js"></script>
    <script src="./js/wow.min.js"></script>
    <script src="./js/jquery.scrollTo.min.js"></script>
    <script src="./js/jquery.nicescroll.js" ></script>
    <script src="./js/jquery.app.js"></script>
    <script src="./js/moment.min.js"></script>
    <!-- Dashboard -->
   
    <!-- Todo -->
    <script src="./js/jquery.todo.js"></script>
    <!-- sweet alerts -->
    <script src="./js/sweet-alert.min.js"></script>
    <script src="./js/sweet-alert.init.js"></script>
    <script src="./js/jquery.dataTables.min.js"></script>
    <script src="./js/dataTables.responsive.min.js"></script>
    <script src="./js/dataTables.bootstrap.min.js"></script>
     <!--Morris Chart-->
    <script src="./js/menu.js"></script>
    <script src="./js/venta.js"></script>
    <script src="./js/reporteVenta.js"></script>
    </body>
</html>
