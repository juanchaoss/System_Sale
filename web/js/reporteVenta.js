/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global date */

$(document).ready(function() 
{
   //formateo de fecha
    function formatDate(fechaInicio){
        var x = date.split("-");
        var newDate = x[2]+'-'+x[1]+'-'+x[0];
        return newDate;
    } 
    $('#fechaInicio').datetimepicker({
        format: 'DD-MM-YYYY',
        showTodayButton: true,
        tooltips: {
            today: 'Ir al día de hoy',
            clear: 'Limpiar selección',
            close: 'Cerrar ventana',
            selectMonth: 'Seleccionar mes',
            prevMonth: 'Mes anterior',
            nextMonth: 'Próximo mes',
            selectYear: 'Seleccionar año',
            prevYear: 'Año anterior',
            nextYear: 'Próximo año'
        }
    });
     function formatDate(fechaFin){
        var x = date.split("-");
        var newDate = x[2]+'-'+x[1]+'-'+x[0];
        return newDate;
    } 
    $('#fechaFin').datetimepicker({
        format: 'DD-MM-YYYY',
        showTodayButton: true,
        tooltips: {
            today: 'Ir al día de hoy',
            clear: 'Limpiar selección',
            close: 'Cerrar ventana',
            selectMonth: 'Seleccionar mes',
            prevMonth: 'Mes anterior',
            nextMonth: 'Próximo mes',
            selectYear: 'Seleccionar año',
            prevYear: 'Año anterior',
            nextYear: 'Próximo año'
        }
    });
    
});


