var dataArray = [];

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();
    $("#img").hide();
    $("#print").hide();
    $("#volver").hide();
    
    function addDataArray(value) {
        dataArray.push({
            numbers: value.numbers,
            amount: value.amount,
            prize: value.prize,
            sstatus: value.sstatus,
            miniSerial: value.miniSerial,
            confirmationLine: value.confirmationLine,
            serialMum: value.serialMum,
            serial: value.serial,
            draw: value.draw,
            date: value.date
        });
    }
    
    //formateo de fecha
    function formatDate(date){
        var x = date.split("-");
        var newDate = x[2]+'-'+x[1]+'-'+x[0];
        return newDate;
    }
    
    $('#date').datetimepicker({
        format: 'DD-MM-YYYY',
        showTodayButton: true,
        tooltips: {
            today: 'Ir al día de hoy',
            clear: 'Limpiar selección',
            close: 'Cerrar ventana',
            selectMonth: 'Seleccionar mes',
            prevMonth: 'Mes anterior',
            nextMonth: 'Próximo mes',
            selectYear: 'Seleccionar año',
            prevYear: 'Año anterior',
            nextYear: 'Próximo año'
        }
    });

    $("#card").flip({
        axis: 'y',
        trigger: 'click',
        reverse: true
    });
    
    $("#form").submit(function(e){

        e.preventDefault();

        var date  = $("#date").val();
        var code  = $("#code").val();

        var finalDate = formatDate(date);
        var dataForm = {date: finalDate, code: code};
        var url  = './VoucherServlet1';
      
        var result = sendData(url,dataForm);
        dataArray = [];
        $("#navBets").html("");
        $(".round").html("");
        if(result !== null && result.data !== undefined){
            $("#content").hide();
            $("#img").show();
            $("#print").show();
            $("#volver").show();
            createNav(result.data);
        }else
           swal("Los datos ingresados son incorrectos", "Por favor intente de nuevo", "error");
    });
    
    function sendData(url,dataForm){
        var response = null;
        $.ajax({
            type: 'POST',
            url :  url,
            data: dataForm,
            cache: false,
            async: false,
            success:function(data, textStatus,jqXHR){
                response = data;

            },
            error:function(jqXHR, testStatus, errorThrown){
                alert(errorThrown);
            }
        });
        return response;
    }

    function createTr(i,value){
        var html ='<tr>'+
            '<th scope="row">'+i+'</th>'+
            '<td>'+value.numbers+'</td>'+
            '<td>'+value.amount+'</td>'+
            '<td>'+value.prize+'</td>'+
            '</tr>';
        return html;
    }

    function createNav(data){
        var i = 0;
        var tr = "";
        $.each(data, function(key, value) {
            ++i;
            addDataArray(value);
            var prize = parseFloat(value.prize);
            if (prize > 0){
                tr += createTr(i,value);
            }
        });
        $("#detailsPrizes").html(tr!==""? createTablePrize(tr):NoWinner());
        $('#navBets').bootpag({
            total: dataArray.length,
            page: 1,
            maxVisible: 10,
            leaps: true,
            firstLastUse: true,
            first: '<span aria-hidden="true">&larr;</span>',
            last: '<span aria-hidden="true">&rarr;</span>',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function(event, num){
            showPage(num);
        }).find('.pagination');
        showPage(1);
    }
    function createTablePrize(trDetail){
        var html ='<div class="panel panel-success">\n'+
            '<div class="panel-heading">'+
            '<b>FELICIDADES, El ticket es ganador.</b>'+
            '</div>'+
            '<div class="panel-body">'+
            '<p>A contuniaci&oacute;n se detallan la(s) juagadas ganadora(s) que posee el ticket</p>'+
            '</div>'+
            '<table class="table">'+
            '<thead>'+
            '<tr>'+
            '<th>#</th>'+
            '<th>Apuesta</th>'+
            '<th>Bs. Apostados</th>'+
            '<th>Bs. Premios</th>'+
            '</tr>'+
            '</thead>'+
            '<tbody>'+
            trDetail+
            '</tbody>'+
            '</table>'+
            '</div>';
        return html;
    }

    function NoWinner(){
        var html ='<div class="panel panel-danger">\n'+
            '<div class="panel-heading">'+
            '<b>El ticket no es ganador</b>'+
            '</div>'+
            '<div class="panel-body">'+
            '<p>Suerte para la proxima, sigue intentando</p>'+
            '</div>'+
            '</div>';
        return html;
    }

    function showBets(bet,pag){
        var n = bet.numbers.length / 2;
        var kino = bet.numbers;
        var x = 15 - n;
        for(var i=1; i <= n;i++){
            var ball   = kino.substring(0,2);
            kino = kino.substring(2);
            var sorteo = bet.draw+"-"+bet.date;
            $("#box"+(i+x)).html(ball);
            $("#numbers").text(bet.numbers);
            $("#miniSerial").text(bet.miniSerial);
            $("#serial").text(bet.serial);
            $("#sorte").text(sorteo);
           /* JsBarcode("#barcode1", "Hi world!");
            JsBarcode("#barcode2", "Hi world!");*/
        }
        $("#textTicketJug").html($("#ticketCod").val() +" - "+ pag);
    }

    function clearBets(){
        $(".round").html("");

    }

    function showPage(pageIndex){
        if(dataArray.length > 0){
            var bet = dataArray[pageIndex-1];
            clearBets();
            showBets(bet,pageIndex);
        }
    }

    $("#print").click(function(){
       /* $("#front,#back").printThis({
            debug:false,
            importCSS: true,
            pageTitle: "Resultado de la Busqueda",
            header: "<h2>Boleto del Kino</h2>",
            loadCSS:"../css/style.css"
        });*/
        window.print();
    });
});