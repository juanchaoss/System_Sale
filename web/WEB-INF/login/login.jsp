<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="">
        <title>Mycrocom</title>
        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>
        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet"/>
        <link href="./css/bootstrap-reset.css" rel="stylesheet"/>
        <!--Animation css-->
        <link href="./css/animate.css" rel="stylesheet"/>
        <!--Icon-fonts css-->
        <link href="./css/font-awesome.css" rel="stylesheet" />
        <link href="./css/ionicons.min.css" rel="stylesheet" />
        <!--Morris Chart CSS -->
        <link href="./css/morris.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="./css/style.css" rel="stylesheet"/>
        <link href="./css/helper.css" rel="stylesheet"/>
        <link href="./css/style-responsive.css" rel="stylesheet" />
        
    </head>
    <body>
        <div class="wrapper-page animated fadeInDown">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                   <h3 class="text-center m-t-10"> Bienvenido <strong>Mycrocom</strong> </h3>
                </div> 
                <form class="form-horizontal m-t-40" action="">                  
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Usuario">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" placeholder="Contraseņa">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <label class="cr-styled">
                                <input type="checkbox" checked>
                                <i class="fa"></i> 
                                Recuerdame
                            </label>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <div class="col-xs-12">
                            <button class="btn btn-success w-md" type="submit">Log in</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="./js/jquery.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script src="./js/pace.min.js"></script>
        <script src="./js/wow.min.js"></script>
        <script src="./js/jquery.nicescroll.js" type="text/javascript"></script>
        <!--common scrit for all pages-->
        <script src="./js/jquery.app.js"></script>
    
    </body>
</html>
