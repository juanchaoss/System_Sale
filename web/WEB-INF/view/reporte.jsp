<div class="modal fade bd-example-modal-lg"  id="myModal" tabindex="-1" role="dialog" style="font-size: 9px;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detalle de venta</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">

                            <div class="col-lg-3">
                                <span class="" id="nFact" name="nFact"><b>N� de Factura:</b> 0000001</span>
                            </div>
                            <div class="col-lg-3">
                                <span class="" id="nFact" name="nFact"><b>Fecha:</b> 18/04/2016</span>
                            </div>
                            <div class="col-lg-3">
                                <span class="" id="nFact" name="nFact"><b>Agencia:</b> La bendicion de dios, c.a</span>
                            </div>
                            <div class="col-lg-3">
                                <span class="" id="nFact" name="nFact"><b>Direccion:</b>4to de mayo, lallllala</span>
                            </div>
                            <br><br>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th>N� Comprobante</th>
                                                <th>Sorteo</th>
                                                <th>N� de tickets</th>
                                                <th>Miniserial</th>
                                                <th>Numeros</th>
                                                <th>Precio</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>000000008</td>
                                                <td>1251-11/07/2016</td>
                                                <td>1</td>
                                                <td>559</td>
                                                <td>010203040506070809101112131415</td>
                                                <td>400</td>
                                            </tr>

                                        </tbody>

                                    </table>
                                </div>

                            </div>

                            <div class="col-lg-12">
                                <span class="form-control" id="nFact" name="nFact"><b>TOTAL: </b>BS.400</span>
                            </div>
                            <br><br>
                        </div>

                    </div>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="print" onclick="print();" class="btn btn-success"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="panel panel-default">
    <div class="panel-heading"><center><h2>REPORTES DE VENTA</h2></center></div>
    <div class="panel-body">

        <br>
        <form class="form-horizontal">
            <div class="form-group">
                <div class="col-lg-2">
                    <label> Fecha Inicio</label>
                    <input type="text" class="form-control" placeholder="DD-MM-AAAA"  id="fechaInicio" name="fechaInicio"> 
                </div>
                <div class="col-lg-2">
                    <label> Fecha Fin</label>
                    <input type="text" class="form-control" placeholder="DD-MM-AAAA" id="fechaFin" name="fechaFin"> 
                </div>
                <div class="col-lg-2">
                    <label> N� de Factura</label>
                    <input type="text" class="form-control" id="nComp" name="nComp" placeholder=""> 
                </div>
                <div class="col-lg-2" style="padding-top: 25px;" ><a href="#" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Buscar</a></div>
                <div class="col-lg-2"  style="padding-top: 25px;" ><a href="#" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Exportar</a></div>
                <div class="col-lg-2"  style="padding-top: 25px;" ><a href="#" class="btn btn-success"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Imprimir</a></div>
            </div>
        </form>
        <br><br>
        <div class="table-responsive">
            <table id="tRventas" name="tRventas" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>N� Factura</th>
                        <th>Fecha</th>
                        <th>Agencia</th>
                        <th>Ubicaci�n</th>
                        <th>Cantidad de tickets</th>
                        <th>Detalle de venta</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Column content</td>
                        <td>Column content</td>
                        <td>Column content</td>
                        <td>Column content</td>
                        <td>Column content</td>
                        <td><a id="detalle" name="detalle" class="btn fa fa-search-plus" aria-hidden="true"  type="button" data-toggle="modal" data-target="#myModal"></a></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>





