package base.servlet;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Eduanny
 */
@WebServlet(name = "IndexServlet", urlPatterns = {"/IndexServlet"})
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        request.getRequestDispatcher("/WEB-INF/view/welcome.jsp").forward(request,response);
    }
    
      @Override
     protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        String options = request.getParameter("opt");
         try {
             switch(options){
                 
                case "Kino": Kino(request, response); break;
                case "Reportes": Reportes(request, response); break;
                case "Lottoleon": Lottoleon(request, response); break;
                case "ReportesLotto": ReportesLotto(request, response); break;
               
             }
        } catch (Exception ex) {
            Logger.getLogger(IndexServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      private void Kino(HttpServletRequest request, HttpServletResponse response)  throws Exception{
       getServletContext().getRequestDispatcher("/WEB-INF/view/VentaKino.jsp").forward(request, response);
    }
       private void Reportes(HttpServletRequest request, HttpServletResponse response)  throws Exception{
       getServletContext().getRequestDispatcher("/WEB-INF/view/reporte.jsp").forward(request, response);
    }
         private void Lottoleon(HttpServletRequest request, HttpServletResponse response)  throws Exception{
       getServletContext().getRequestDispatcher("/WEB-INF/view/LottoLeon.jsp").forward(request, response);
    }
        private void ReportesLotto(HttpServletRequest request, HttpServletResponse response)  throws Exception{
       getServletContext().getRequestDispatcher("/WEB-INF/view/reportelotto.jsp").forward(request, response);
    }
    
}
