package base.servlet;

import base.model.Voucher;
import base.model.Kino;
import base.model.KinoPlay;
//import base.model.Report;
import base.service.VoucherService;
//import base.service.ReportService;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author JuanBarreto
 */
@WebServlet(name = "ClientSaleServlet", urlPatterns = {"/ClientSaleServlet"})
public class ClientSaleServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String jsonString = request.getParameter("jsonString");
        System.out.println(jsonString);
//        String dateBegin = request.getParameter("dateBegin");
//        String dateEnd = request.getParameter("dateEnd");
//        String envoice = request.getParameter("envoice");
        String action = request.getParameter("action");
        String idPos = request.getParameter("idPos");
        if (!action.isEmpty() && "getRandom".equals(action)) {
            try {
                String newNumber = getRandom();
                String finalNumber = formatOutputWithComa(newNumber);
                //message = "Comprobante generado: "+answer;
                String strJson = new Gson().toJson(finalNumber);
                sendJSONMessage(response, strJson);
            } catch (Exception ex) {
                Logger.getLogger(ClientSaleServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (!action.isEmpty() && "insert".equals(action)) {
            try {
                List<KinoPlay> list = convertJsonToArray(jsonString);
                if (list.size() > 0) {
                    VoucherService controller = new VoucherService();
                    long answer = controller.saveSale(list);

                    String message = "";
                    if (answer > 0) {
                        //List<Kino> kinoPlayList = voucher.getKinoList();
                        //Object kinoList = null;
                        message = "Comprobante generado: " + answer;
                        String strJson = "\"resp\":{\"ok\":" + new Gson().toJson(message) + "}";
                        sendJSONMessage(response, strJson);
                    } else {
                        message = "Ha ocurrido un error al guardar, intentelo mas tarde";
                        String strJson = "\"resp\":{\"error\":" + new Gson().toJson(message) + "}";
                        sendJSONMessage(response, strJson);
                    }
                }
                /* JsonElement json = new JsonParser().parse(jsonString);
            JsonArray array = json.getAsJsonArray();
            Gson gson = new Gson();
            Iterator<JsonElement> iterator = array.iterator();
            while(iterator.hasNext()){
            JsonElement jsonElement =  (JsonElement) iterator.next();
            
            KinoPlay kinoPlay1 = (KinoPlay) gson.fromJson(jsonElement, KinoPlay.class);
            //kinoPlay1.getId()+" - "+kinoPlay1.getPlay()
            System.out.println(kinoPlay1);
            //String value =  array.
            //  System.out.println("key "+ key);
            }*/
            } catch (Exception ex) {
                Logger.getLogger(ClientSaleServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else if(!action.isEmpty() && "getInfoVou".equals(action)){
            List finalInf = new ArrayList();
            VoucherService controller = new VoucherService();
            int id = Integer.parseInt(idPos);
            try {
                finalInf = controller.getInfoToVoucher(id);
                
            } catch (Exception ex) {
                Logger.getLogger(ClientSaleServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

//        if(!opc.isEmpty()){
//            try {
//                validateCaseReport(opc, dateBegin, dateEnd, envoice,response);
//            } catch (Exception ex) {
//                Logger.getLogger(ClientSaleServlet.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    private String formatOutputWithComa(String x) throws Exception {
        //String outFormated = "";
        StringBuilder outFormated = new StringBuilder();

        for (int i = 1, n = x.length() / 2; i <= n; i++) {
            String z = x.substring(0, 2);
            x = x.substring(2);
            outFormated.append(z).append(i==15?"":",");
        }
        return outFormated.toString();
    }

    private List<KinoPlay> convertJsonToArray(String dataJson) throws Exception {

        List<KinoPlay> kinoPlayList = new ArrayList();
        JsonElement json = new JsonParser().parse(dataJson);
        JsonArray array = json.getAsJsonArray();
        Gson gson = new Gson();
        Iterator<JsonElement> iterator = array.iterator();
        while (iterator.hasNext()) {
            JsonElement jsonElement = (JsonElement) iterator.next();
            KinoPlay kinoPlay1 = (KinoPlay) gson.fromJson(jsonElement, KinoPlay.class);
            kinoPlayList.add(kinoPlay1);
            //kinoPlay1.getId()+" - "+kinoPlay1.getPlay()
            //System.out.println(kinoPlay1);
            //String value =  array.
            //  System.out.println("key "+ key);
        }
        return kinoPlayList;
    }

    /*private void validateCaseReport(String opc,String dateBegin,String dateEnd,String envoice,HttpServletResponse response) throws Exception{
        
        if("report".equals(opc)){
            
            ReportService controller = new ReportService();
            
            if (dateBegin.length() > 0 || dateEnd.length() > 0 || envoice.length() > 0) {
                try {
                    List<Report> reportList = controller.getSummaryInvoice();
                    String reportListJson = new Gson().toJson(reportList);
                    sendJSONMessage(response, reportListJson);
                } catch (Exception ex) {
                    Logger.getLogger(ClientSaleServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
               try {
                    List<Report> reportList = controller.getSummaryInvoice(dateBegin,dateEnd,envoice);
                    String reportListJson = new Gson().toJson(reportList);
                    sendJSONMessage(response, reportListJson);
                } catch (Exception ex) {
                    Logger.getLogger(ClientSaleServlet.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        }
    }*/
    private void sendJSONMessage(HttpServletResponse response, String jsonMessage) throws IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        System.out.println(jsonMessage);
        out.print(jsonMessage);
        out.flush();
    }

    private String getRandom() {
        String result = "";
        VoucherService controller = new VoucherService();
        try {
            result = controller.getRandomNumbers();
        } catch (Exception ex) {
            Logger.getLogger(ClientSaleServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
