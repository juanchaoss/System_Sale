//package base.servlet;
//
//import com.google.gson.Gson;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.ArrayList;
//import java.util.List;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// *
// * @author juanchaoss
// */
//public class VoucherServlet extends HttpServlet {
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        //processRequest(request, response);
//        String ticketCod = request.getParameter("ticketCod");
//        String day = request.getParameter("day");
//        String errorMsg = evalErrorParameters(ticketCod, day);
//        if (errorMsg != null) {
//            sendJSONMessage(response, jsonMessage(null, errorMsg));
//        } else {
//            try {
//                String theDay = parseDay(day);
//                if (theDay != null) {
//                    searchTicket(request, response, ticketCod, theDay);
//                } else {
//                    sendJSONMessage(response, jsonMessage(null, "Fecha incorrecta"));
//                }
//            } catch (Exception e) {
//                sendJSONMessage(response, jsonMessage(null, e.getMessage()));
//            }
//        }
//    }
//
//    private void searchTicket(HttpServletRequest request, HttpServletResponse response, String ticketCod, String day) throws Exception {
//        Ticket ticket = ticketService.find(ticketCod, day);
//        if (ticket == null) {
//            sendJSONMessage(response, jsonMessage(null, "Comprobante no encontrado"));
//        } else {
//            sendJSONMessage(response, jsonMessage(ticket, null));
//        }
//    }
//
//    private String evalErrorParameters(String ticketCod, String day) {
//        String str = null;
//        if (ticketCod == null) {
//            str = "C&oacute;digo de ticket incorrecto.";
//        }
//
//        if (day == null) {
//            str += (str != null ? "<br>" : "") + "Fecha incorrecta.";
//        }
//
//        return str;
//    }
//
//    private String parseDay(String day) {
//        String date = day.replace("/", "").replace("-", "");
//        if (date.length() == 8) {
//            String dd = date.substring(0, 2);
//            date = date.substring(2);
//            String mm = date.substring(0, 2);
//            date = date.substring(2);
//            return date + mm + dd;
//        } else {
//            return null;
//        }
//    }
//
//    private String jsonMessage(Ticket ticket, String error) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("{");
//        if (ticket != null) {
//            List<Bet> betsList = new ArrayList<Bet>();
//            betsList = ticket.getBetsList();
//            sb.append("\"data\":").append(new Gson().toJson(betsList));
//        } else if (error != null) {
//            sb.append("\"error\":").append("\"").append(error).append("\"");
//        }
//        sb.append("}");
//        return sb.toString();
//    }
//
//    public static void sendJSONMessage(HttpServletResponse response, String message) throws IOException {
//        response.setContentType("application/json");
//        PrintWriter out = response.getWriter();
//        System.out.println(message);
//        out.print(message);
//        out.flush();
//    }
//}
