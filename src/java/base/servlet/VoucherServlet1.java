package base.servlet;

import base.model.Kino;
import base.model.Voucher;
import base.service.VoucherService;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author JuanBarreto
 */
@WebServlet(name = "VoucherServlet1", urlPatterns = {"/VoucherServlet1"})
public class VoucherServlet1 extends HttpServlet {


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String date = request.getParameter("date");
        String code = request.getParameter("code");
        VoucherService controller = new VoucherService();
        
        try {
            Voucher voucher =  controller.find(code, date);
            if (voucher != null) {
                List<Kino> kinoList = voucher.getKinoList();
                String strJson = "{\"data\":"+new Gson().toJson(kinoList)+"}";
                sendJSONMessage(response, strJson);
            }else{
                String str = "{\"error\": \"no hay resultados para ese boleto.\"}";
                sendJSONMessage(response, str);
            }
        } catch (Exception ex) {
            Logger.getLogger(VoucherServlet1.class.getName()).log(Level.SEVERE, null, ex);
                String str = "{\"error\": \"entro en excepcion\"}";
               sendJSONMessage(response, str);
        }
        
    }
    
    private void sendJSONMessage(HttpServletResponse response, String jsonMessage) throws IOException{
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        System.out.println(jsonMessage);
        out.print(jsonMessage);
        out.flush();
    }
}
