package base.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

/**
 *
 * @author Yordanys Pupo
 */
public abstract class AbstractDAO {
        
    protected Connection connection;
    private static String    LOCAL_SERVER_NAME = "10.0.1.109";
    private static int       LOCAL_PORT = 3306;
    private static String    LOCAL_DATA_BASE = "lotterydb2";
    private static String    LOCAL_USER_NAME = "dbamycrocom";
    private static String    LOCAL_PASSWORD = "mycrocomsql";
    private static String    LOCAL_DRIVER_NAME = "com.mysql.jdbc.Driver";
            
    protected static final String timeoutError = "Tiempo de espera agotado para acceder al servidor de datos.";
    protected static final String sqlError = "Ha ocurrido un error de SQL al intentar acceder al servidor de datos.";
    
    private static final String driverNotFoundError = "El driver especificado para la conexión no se ha encontrado.";
    private static final String failedOpenConnectionError = "No se ha podido establecer conexión con el servidor de datos.\nPor favor, configuere los parámetros de conexión del servidor.";
    private static final String failedCloseConnectionError = "Imposible cerrar la conexión con el servidor de datos.";
    private static final String failedCloseStatementError = "La sentencia SQL para esta acción no se pudo cerrar.";
    private static final String failedCloseResultSetError = "El resultado obtenido de la consulta SQL no se pudo cerrar.";
    private static final String failedRollbackError = "La transacción no se pudo revertir.";
    
    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat fullSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
    public static final SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public static final SimpleDateFormat mysqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat intDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    public static final SimpleDateFormat shortIntDateFormat = new SimpleDateFormat("yyyyMMdd");
        
    public AbstractDAO() {
        
    }
    
    public static void updateParam(String SERVER_NAME, int PORT, String DATA_BASE, String USER_NAME, String PASSWORD, String DRIVER_NAME) {
        AbstractDAO.LOCAL_SERVER_NAME = SERVER_NAME;
        AbstractDAO.LOCAL_PORT = PORT;
        AbstractDAO.LOCAL_DATA_BASE = DATA_BASE;
        AbstractDAO.LOCAL_USER_NAME = USER_NAME;
        AbstractDAO.LOCAL_PASSWORD = PASSWORD;
        AbstractDAO.LOCAL_DRIVER_NAME = DRIVER_NAME;
    }
        
    protected void getConnecton(boolean transactional) throws Exception {
        try {                    
            Class.forName(LOCAL_DRIVER_NAME);
            connection = DriverManager.getConnection("jdbc:mysql://"+ LOCAL_SERVER_NAME + ":" + LOCAL_PORT + "/" + LOCAL_DATA_BASE, LOCAL_USER_NAME, LOCAL_PASSWORD);
            connection.setAutoCommit(!transactional);
        } catch (ClassNotFoundException ex) {
            throw new Exception(driverNotFoundError);
        } catch (SQLException ex) {
            throw new Exception(failedOpenConnectionError);
        }        
    }
       
    protected void closeConnection() throws Exception {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw new Exception(failedCloseConnectionError);
            }
        }
    }
    
    protected void closeStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                System.err.println(failedCloseStatementError);
            }
        }
    }
    
    protected void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                System.err.println(failedCloseResultSetError);
            }
        }
    }
    
    protected void revertTransaction() {
        try {                
            connection.rollback();
        } catch (SQLException ex) {
            System.err.println(failedRollbackError);
        }
    }      
}
