package base.dao;

import base.model.Voucher;
import base.model.Kino;
import base.model.KinoPlay;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author JuanBarreto
 *
 */
public class VoucherDAO extends AbstractDAO {

    private static final String FIND_SQL = "{CALL searchVoucher(?,?)}";
    private static final String INSERT_KINO_SQL = "INSERT INTO SALE_DETAIL (ID_SALE,ID_DRAW,NUMBER,MINISERIAL,STATUS) VALUES (?,?,?,?,?)";
    //private static final String GET_PRICE_TICKET = "SELECT PRICE_TICKET FROM DRAW";
    private static final String GET_RANDOM_SQL = "SELECT A.NUMBER FROM SALE_KINO AS A LEFT JOIN SALE_DETAIL AS B ON (A.NUMBER=B.NUMBER) LIMIT 1";
    //private static final String COMPLETE_NUMBER = "SELECT A.NUMBER FROM SALE_KINO AS A LEFT JOIN SALE_DETAIL AS B ON (A.NUMBER=B.NUMBER) LIMIT 1";
    private static final String SAVE_SALE_SQL = "INSERT INTO SALE (ID_POS,ID_AGENCY,ID_COMPANY,SALE_COST,SALE_COUNT,STATUS) VALUES (?,?,?,?,?,?)";
    private static final String GET_LAST_ID_DRAW = "SELECT MAX(ID_DRAW) AS ID FROM DRAW";
    private static final String GET_INFO_VOUCHER =  "SELECT A.POS_NAME, B.AGENCY_NAME, C.COMPANY_NAME, D.PRODUCT_NAME, E.PRICE_TICKET\n" +
                                                    "FROM POS A JOIN AGENCY B ON A.ID_POS=? AND A.ID_AGENCY=B.ID_AGENCY JOIN COMPANY C USING(ID_COMPANY) LEFT JOIN PRODUCT D ON C.ID_COMPANY=D.ID_COMPANY AND PRODUCT_NAME = 'KINO'\n" +
                                                    "JOIN(SELECT PRICE_TICKET FROM DRAW ORDER BY ID_DRAW DESC LIMIT 1) AS E;";
    /*SQLSOLDNUMBER*/
    private static final String IS_SOLD_NUMBER = "SELECT A.ID,A.NUMBER FROM (SELECT ? AS ID, CONVERT(? USING utf8) AS NUMBER ";
    private static final String INNER_SOLD_SQL = "UNION ALL SELECT ? AS ID, CONVERT(? USING utf8) AS NUMBER ";
    private static final String END_QUERY_SOLD_SQL = " ) AS A, SALE_DETAIL B WHERE A.NUMBER=B.NUMBER";
    /*END - SQLSOLDNUMBER*/
    private String isValidAmountFormat(String amount) {
        double amountAux = Double.parseDouble(amount);
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", formatSymbols);
        return decimalFormat.format(amountAux);
    }

    /*public String getPriceTicket() throws Exception {
        String result = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            getConnecton(false);
            ps = connection.prepareStatement(GET_PRICE_TICKET);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getString("PRICE_TICKET");
            }
        } catch (Exception e) {
        }
        return result;
    }*/
    public List getInfoToVoucher(int idPos) throws Exception{
        List info = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        getConnecton(false);
        ps = connection.prepareStatement(GET_INFO_VOUCHER);
        ps.setInt(1, idPos);
        rs = ps.executeQuery();
        if(rs.next()){
            info.add(rs.getString("POS_NAME"));
            info.add(rs.getString("AGENCY_NAME"));
            info.add(rs.getString("COMPANY_NAME"));
            info.add(rs.getString("PRODUCT_NAME"));
            info.add(rs.getString("PRICE_TICKET"));
        }
        return info;
    }
    
    public Voucher find(String voucherCod, String day) throws Exception {
        Voucher result = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        double totalAmount = 0;
        List<Kino> kinoList = new ArrayList();
        try {
            getConnecton(false);
            cs = connection.prepareCall(FIND_SQL);
            cs.setString(1, day);
            cs.setString(2, voucherCod);
            rs = cs.executeQuery();
            while (rs.next()) {
                Kino k = new Kino(rs.getDouble("AMOUNT"), rs.getString("STATUS"), rs.getString("DRAW_DATE"), rs.getString("MINISERIAL"), rs.getString("NUMBER"), rs.getString("DRAW_NUMBER"), rs.getString("SERIAL_TICKET"), rs.getString("PRICE_TICKET"));
                kinoList.add(k);
                totalAmount += rs.getDouble("AMOUNT");
            }
            if (kinoList.size() > 0) { //size o empty
                result = new Voucher(voucherCod, kinoList, totalAmount);
            }
        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {

            throw new Exception(sqlError);
        } finally {
            closeResultSet(rs);
            closeStatement(cs);
            closeConnection();
        }
        return result;
    }//METODO LISTO

    public long saveSale(List<KinoPlay> kinoPlayList) throws Exception {

        //Voucher result = null;
        PreparedStatement ps = null;
        List<KinoPlay> listFaild;
        List<KinoPlay> listSuccess = null;
        listFaild = isSoldNumbers(kinoPlayList);
        long id = 0;
        /*listSuccess.addAll(kinoPlayList);
        listSuccess.removeAll(listFaild);*/ //<- ARREGLAR ESTO
        long idSale = saveSaleFirst(kinoPlayList);

        int idDraw = getLastIdDraw();

        if (kinoPlayList.size() > 0) {
            try {
                getConnecton(false);
                ps = connection.prepareStatement(INSERT_KINO_SQL);

                for (int i = 0, n = kinoPlayList.size(); i < n; i++) {

                    KinoPlay info = kinoPlayList.get(i);
                    ps.setLong(1, idSale);
                    ps.setInt(2, idDraw);
                    //ps.setString(3,"0.369.369");
                    ps.setString(3, info.getPlay());
                    ps.setString(4, info.getMiniSerial());
                    ps.setString(5, "2");
                    ps.addBatch();
                }
                System.out.println(ps);
                ps.executeBatch();

            } catch (SQLTimeoutException sqltoe) {
                throw new Exception(sqltoe);
            } catch (SQLException sqle) {
                throw new Exception(sqle);
            } finally {
                closeStatement(ps);
                closeConnection();
            }
        }
        return idSale;
    }//METODO LISTO

    public String getRandomNumbers() throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        String result = null;
        getConnecton(false);

        try {
            ps = connection.prepareStatement(GET_RANDOM_SQL);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getString("NUMBER");
            }
        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {
            throw new Exception(sqlError);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
            closeConnection();
        }
        return result;
    }

    public List<KinoPlay> isSoldNumbers(List<KinoPlay> kinoPlayList) throws Exception {

        List<KinoPlay> listFaild = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int n = kinoPlayList.size(), x = 0;
        String sql = buildSql(n);
        try {
            getConnecton(false);
            ps = connection.prepareStatement(sql);

            for (int i = 0; i < n; i++) {
                KinoPlay info = kinoPlayList.get(i);
                ps.setString(++x, info.getId());
                ps.setString(++x, info.getPlay());
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                listFaild.add((KinoPlay) rs);
            }
        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {
            throw new Exception(sqlError);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
            closeConnection();
        }
        // System.out.println(listFaild);
        return listFaild;
    }//METODO LISTO

    public long saveSaleFirst(List<KinoPlay> KinoPlayList) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int countCost = KinoPlayList.size();
        int saleCost = KinoPlayList.size() * 400;
        String status = "3";
        int y = countCost;
        int x = 3;
        long id = 0;
        try {

            getConnecton(false);
            ps = connection.prepareStatement(SAVE_SALE_SQL, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setInt(1, 1);
            ps.setInt(2, 1);
            ps.setInt(3, 1);
            ps.setInt(4, saleCost);
            ps.setInt(5, countCost);
            ps.setString(6, status);
            //ps.addBatch();

            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {
            throw new Exception(sqlError); //llega hasta aqui
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
            closeConnection();
        }
        return id;
    }//METODO LISTO

    public String buildSql(int n) {

        StringBuilder sb = new StringBuilder(IS_SOLD_NUMBER);
        for (int i = 1; i < n; i++) {
            sb.append(INNER_SOLD_SQL);
        }
        sb.append(END_QUERY_SOLD_SQL);
        return sb.toString();
    }//METODO LISTO

    public int getLastIdDraw() throws Exception {
        int id = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            getConnecton(false);
            ps = connection.prepareStatement(GET_LAST_ID_DRAW);
            rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt("ID");
            }
        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {
            throw new Exception(sqlError);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
            closeConnection();
        }
        return id;
    }
    
    /*public int getLastIdSale() throws Exception{
        int id = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            getConnecton(false);
            ps = connection.prepareStatement(GET_LAST_ID_SALE);
            rs = ps.executeQuery();
            if (rs.next()) {        
                id = rs.getInt("ID");
            } 
        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {
            throw new Exception(sqlError);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
            closeConnection();
        }
        return id;
    }*/
}
