package base.test;

import base.dao.AbstractDAO;
import base.model.Voucher;
import base.service.VoucherService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juanchaoss
 */
public class Test {
    public static void main(String[] args) {

        AbstractDAO.updateParam("10.0.1.109", 3306, "mycrocomLotterydb", "dbamycrocom", "mycrocomsql", "com.mysql.jdbc.Driver");
        VoucherService servicio = new VoucherService();
        try {
          Voucher inf = servicio.find("2016-02-14","000001");
          System.out.println(inf.getKinoList().size());
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
