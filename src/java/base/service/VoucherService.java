package base.service;

import base.dao.VoucherDAO;
import base.model.KinoPlay;
import base.model.Voucher;
import java.util.List;
/**
 *
 * @author JuanBarreto
 */
public class VoucherService {
    private VoucherDAO voucherDAO;

    public VoucherService() {
        this.voucherDAO = new VoucherDAO();
    }
    
    public Voucher find(String voucherCod,String day) throws Exception {
      return voucherDAO.find(voucherCod, day);
    }
    
    /*public String getPriceTicket() throws Exception {
        return voucherDAO.getPriceTicket();
    }*/
    
    public List<KinoPlay> isSoldNumbers(List<KinoPlay> kinoPlayList) throws Exception {
        return voucherDAO.isSoldNumbers(kinoPlayList);
    }
    
    public String getRandomNumbers() throws Exception {
        return voucherDAO.getRandomNumbers();
    }
    
    public long saveSale(List objectArray) throws Exception {
        return voucherDAO.saveSale(objectArray);
    }
    
    public List getInfoToVoucher(int idPos) throws Exception{
        return voucherDAO.getInfoToVoucher(idPos);
    }
}
