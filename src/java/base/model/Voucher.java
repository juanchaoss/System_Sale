/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juanchaoss
 */
public class Voucher {
    private String voucherCod;
    private List<Kino> kinoList;
    private double amountTotal;

    public Voucher(String voucherCod, List<Kino> kinoList, double amountTotal) {
        this.voucherCod  = voucherCod;
        this.kinoList    = kinoList;
        this.amountTotal = amountTotal;
    }

    public Voucher() {
        this.voucherCod = "";
        this.kinoList = new ArrayList();
    }

    public String getVoucherCod() {
        return voucherCod;
    }

    public void setVoucherCod(String voucherCod) {
        this.voucherCod = voucherCod;
    }

    public List<Kino> getKinoList() {
        return kinoList;
    }

    public void setKinoList(List<Kino> kinoList) {
        this.kinoList = kinoList;
    }

    public double getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(double amountTotal) {
        this.amountTotal = amountTotal;
    }
    
}
