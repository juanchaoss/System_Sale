/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.model;

/**
 *
 * @author JuanBarreto
 */
public class Kino {
    
    private double amount;
    private String status;
    private String serial;
    private String date;
    private String miniSerial;
    private String numbers;
    private String confirmationLine;
    private double prize;
    private String draw;
    private double countSuccess;
    private String price;
  
    public Kino() {
    }

    public Kino(double amount, String status, String date, String miniSerial, String numbers, String draw,String serial,String price) {
        this.amount = amount;
        this.status = status;
        this.date = date;
        this.miniSerial = miniSerial;
        this.numbers = numbers;
        this.draw = draw;
        this.serial = serial;
        this.price = price;
    }
    
    public double getPrize() {
        return prize;
    }

    public void setPrize(double prize) {
        this.prize = prize;
    }

    public String getDraw() {
        return draw;
    }

    public void setDraw(String draw) {
        this.draw = draw;
    }

    public double getCountSuccess() {
        return countSuccess;
    }

    public void setCountSuccess(double countSuccess) {
        this.countSuccess = countSuccess;
    }
    
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMiniSerial() {
        return miniSerial;
    }

    public void setMiniSerial(String miniSerial) {
        this.miniSerial = miniSerial;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public String getConfirmationLine() {
        return confirmationLine;
    }

    public void setConfirmationLine(String confirmationLine) {
        this.confirmationLine = confirmationLine;
    }
}