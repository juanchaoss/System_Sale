package base.model;
/**
 * @author JuanBarreto
 */
public class KinoPlay {
    private String id;
    private String play;
    private String miniSerial;

    public KinoPlay(String id, String play, String miniSerial) {
        this.id = id;
        this.play = play;
        this.miniSerial = miniSerial;
    }

    public KinoPlay() {
    }

    public KinoPlay(String id, String play) {
        this.id = id;
        this.play = play;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlay() {
        return play;
    }

    public void setPlay(String play) {
        this.play = play;
    }

    public String getMiniSerial() {
        return miniSerial;
    }

    public void setMiniSerial(String miniSerial) {
        this.miniSerial = miniSerial;
    }
    
    @Override
    public String toString() {
        return id +" - "+ play; 
    }
}
